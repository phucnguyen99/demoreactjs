import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from "connected-react-router";

import * as actions from "../../store/actions";

import './Login.scss';

import { handleLoginApi } from '../../services/userService';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            password: '',
            isShowPassword: false,
            errMessage: ''
        }
    }

    handleOnChangeUsername = (event) => {
        this.setState({
            userName: event.target.value
        }) // to change value
    }


    handleOnChangePassword = (event) => {
        this.setState({
            password: event.target.value
        }) // to change value
    }

    handleLogin = async () => {
        this.setState({ errMessage: '' })
        try {
            let data = await handleLoginApi(this.state.userName, this.state.password)
            console.log(data)
            if (data && data.errCode !== 0) {
                this.setState({ errMessage: data.message })
            }

            if (data && data.errCode === 0) {
                this.props.userLoginSucceed(data.user)
                console.log('login succeed')
            }

        } catch (error) {
            if (error.response) {
                if (error.response.data) {
                    this.setState({ errMessage: error.response.data.message })
                }
            }
        }
    }

    handleShowPassword = () => {
        this.setState({
            isShowPassword: !this.state.isShowPassword
        })

    }

    render() {

        return (
            <div className='login-background'>
                <div className='login-container'>
                    <div className='login-content'>
                        <div className='col-12 login-text'>Login</div>
                        <div className='col-12 form-group login-input'>
                            <label>Username:</label>
                            <input type='text' className='form-control' placeholder='Enter your username'
                                onChange={(event) => this.handleOnChangeUsername(event)} />
                        </div>

                        <div className='col-12 form-group login-input' >
                            <label>Password:</label>
                            <div className='custom-input-password'>
                                <input type={this.state.isShowPassword ? 'text' : 'password'}
                                    className='form-control' placeholder='Enter your password'
                                    onChange={(event) => this.handleOnChangePassword(event)} />

                                <span onClick={() => this.handleShowPassword()}>
                                    <i className={this.state.isShowPassword ? 'fas fa-eye' : 'fas fa-eye-slash'}></i>

                                </span>
                            </div>

                        </div>
                        <div className='col-12' style={{ color: 'red' }}>
                            {this.state.errMessage}
                        </div>

                        <div className='col-12'>
                            <button className='btn-login' onClick={() => { this.handleLogin() }}>Login</button>
                        </div>

                        <div className='col-12'>
                            <span className='forgot-password'>Forgot your password</span>
                        </div>

                        <div className='col-12 text-center'>
                            <span className='text-or-login'>Or login with</span>

                        </div>

                        <div className='col-12 social-login'>
                            <i class="fab fa-google-plus-g google"></i>
                            <i class="fab fa-facebook-f facebook"></i>
                        </div>

                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        language: state.app.language
    };
};

const mapDispatchToProps = dispatch => {
    return {
        navigate: (path) => dispatch(push(path)),
        userLoginSucceed: (userInfo) => dispatch(actions.userLoginSucceed(userInfo))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
