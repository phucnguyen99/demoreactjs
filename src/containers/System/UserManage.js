import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import './UserManage.scss';
import { getAllUser, createNewUser, deleteUser, editUser } from '../../services/userService';
import ModalUser from './ModalUser';
import ModalEditUser from './ModalEditUser';
import { emitter } from '../../utils/emitter';

class UserManage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            users: [],
            isOpenPopupCreateUser: false,
            isOpenPopupEditUser: false,
            userForEdit: {}
        }
    }

    async componentDidMount() {
        await this.getAllUser()
    }

    getAllUser = async () => {
        let res = await getAllUser('ALL')
        if (res && res.errCode === 0) {
            this.setState({
                users: res.users
            })
        }
    }

    handleAddUser = () => {
        this.setState({
            isOpenPopupCreateUser: true
        })
    }

    toggleUserModal = () => {
        this.setState({
            isOpenPopupCreateUser: !this.state.isOpenPopupCreateUser
        })
    }

    toggleEditUserModal = () => {
        this.setState({
            isOpenPopupEditUser: !this.state.isOpenPopupEditUser
        })
    }

    createNewUser = async (data) => {
        try {
            let res = await createNewUser(data)
            if (res && res.errCode !== 0) {
                alert(res.errMessage)
            } else {
                await this.getAllUser()
                this.setState({
                    isOpenPopupCreateUser: false
                })
            }
            emitter.emit('key-event-clear-data')
        } catch (error) {

        }
        console.log('data from popup', data)
    }

    handleDeleteUser = async (userData) => {
        console.log(userData)
        try {
            let res = await deleteUser(userData.id)
            console.log(res.errCode)

            if (res && res.errCode === 0) {
                await this.getAllUser()
            } else {
                alert(res.errMessage)
            }
        } catch (e) {
            console.log(e)
        }
    }


    handleEditUser = async (userData) => {
        console.log('data', userData)
        this.setState({
            isOpenPopupEditUser: true,
            userForEdit: userData
        })
    }

    saveChageUser = async (user) => {
        try {
            let res = await editUser(user)
            console.log('click save userrrr', res)
            if (res && res.errCode === 0) {
                this.setState({
                    isOpenPopupEditUser: false
                })
                await this.getAllUser()
            } else {
                alert(res.errCode)
            }
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        console.log('check users', this.state)
        let users = this.state.users
        return (
            <div className='users-container'>
                <ModalUser
                    isOpen={this.state.isOpenPopupCreateUser}
                    toggleFromParent={this.toggleUserModal}
                    createNewUser={this.createNewUser}
                />
                {
                    this.state.isOpenPopupEditUser &&
                    <ModalEditUser
                        isOpen={this.state.isOpenPopupEditUser}
                        toggleFromParent={this.toggleEditUserModal}
                        currentUser={this.state.userForEdit}
                        editUser={this.saveChageUser}
                    />
                }


                <div className="text-center">Manage users by phuc</div>
                <div className="mx-1">
                    <button className='btn btn-primary px-3'
                        onClick={() => this.handleAddUser()}>Add User</button>
                </div>
                <div className="user-table mt-3 mx-1">

                    <table id="customers">
                        <tr>
                            <th>Email</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                        {users && users.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td>{item.email}</td>
                                    <td>{item.firstName}</td>
                                    <td>{item.lastName}</td>
                                    <td>{item.address}</td>
                                    <td>
                                        <button className='btn-edit' onClick={() => { this.handleEditUser(item) }}><i className='fas fa-pencil-alt'></i></button>
                                        <button className='btn-delete' onClick={() => { this.handleDeleteUser(item) }}><i className='fas fa-trash'></i></button>
                                    </td>
                                </tr>
                            )

                        })}
                    </table>
                </div>

            </div>
        );
    }

}

const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = dispatch => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserManage);
