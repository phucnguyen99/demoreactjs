import axios from "../axios"

const handleLoginApi = (email, password) => {
    return axios.post('/api/login', { email, password })
}

const getAllUser = (inputId) => {
    // template string
    return axios.get(`api/get-all-users?id=${inputId}`)
}

const createNewUser = (data) => {
    return axios.post('api/create-new-user', data)
}

const deleteUser = (userId) => {
    return axios.delete('/api/delete-user', { data: { id: userId } })
}

const editUser = (data) => {
    return axios.put('/api/edit-user', data)
}
export { handleLoginApi, getAllUser, createNewUser, deleteUser, editUser }