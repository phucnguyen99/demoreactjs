import actionTypes from './actionTypes';

export const addUserSuccess = () => ({
    type: actionTypes.ADD_USER_SUCCESS
})



export const userLoginSucceed = (userInfo) => ({
    type: actionTypes.USER_LOGIN_SUCCEED,
    userInfo: userInfo
})

export const userLoginFail = () => ({
    type: actionTypes.USER_LOGIN_FAILED
})

export const processLogout = () => ({
    type: actionTypes.PROCESS_LOGOUT
})